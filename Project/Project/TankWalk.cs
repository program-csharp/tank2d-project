﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    internal class TankWalk
    {
        public static void tankWalk(bool goLeft, bool goRight, bool goUp, bool goDown, PictureBox picturePlayerTank, int speed, int formWidth,int formHeight)
        {
            if (goLeft == true && picturePlayerTank.Left > 0)
                picturePlayerTank.Left -= speed;
            else if (goRight == true && picturePlayerTank.Left < formWidth - picturePlayerTank.Width)
                picturePlayerTank.Left += speed;
            else if (goUp == true && picturePlayerTank.Top > 30)
                picturePlayerTank.Top -= speed;
            else if (goDown == true && picturePlayerTank.Top < formHeight - picturePlayerTank.Height)
                picturePlayerTank.Top += speed;
        }
        public static void bulletWalk(bool goLeft, bool goRight, bool goUp, bool goDown, PictureBox pictureBullet, int speed, int formWidth, int formHeight)
        {
            if (goLeft == true)
            {
                if (pictureBullet.Left > 0)
                    pictureBullet.Left -= speed;
                else
                    goLeft = false;
            }
            else if (goRight == true)
            {
                if (pictureBullet.Left < formWidth - pictureBullet.Width)
                    pictureBullet.Left += speed;
                else
                    goRight = false;
            }
            else if (goUp == true)
            {
                if (pictureBullet.Top > 30)
                    pictureBullet.Top -= speed;
                else
                    goUp = false;
            }
            else if (goDown == true)
            {
                if (pictureBullet.Top < formHeight - pictureBullet.Height)
                    pictureBullet.Top += speed;
                else
                    goDown = false;
            }
        }
    }
}
