﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;
namespace Project
{
    internal class SaveToExcel
    {
        public static void SaveDataToExcel(string redPlayer, string bluePlayer, int redCounter, int blueCounter, string fileName)
        {
            Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            excelApp.SheetsInNewWorkbook = 1;
            Excel.Workbook workBook = excelApp.Workbooks.Add(Type.Missing);
            excelApp.DisplayAlerts = false;
            Excel.Worksheet sheet = (Excel.Worksheet)excelApp.Worksheets.get_Item(1);
            sheet.Name = "Result";

            //Записуємо назви стовпців
            sheet.Cells[1, 1] = "Ім я гравців";
            sheet.Cells[1, 2] = "Кількість балів";
            //Записуємо дані
            sheet.Cells[2, 1] = redPlayer;
            sheet.Cells[2, 2] = redCounter;
            sheet.Cells[3, 1] = bluePlayer;
            sheet.Cells[3, 2] = blueCounter;

            //Форматуємо клітинки
            Excel.Range range1 = sheet.Range[sheet.Cells[1, 1], sheet.Cells[1, 2]];
            range1.Cells.Font.Name = "Arial";
            range1.Cells.Font.Size = 20;
            range1.Cells.Font.Color = ColorTranslator.ToOle(Color.Black);
            range1.Interior.Color = ColorTranslator.ToOle(Color.Green);

            Excel.Range range2 = sheet.Range[sheet.Cells[2, 1], sheet.Cells[3, 2]];
            range2.Cells.Font.Name = "Arial";
            range2.Cells.Font.Size = 14;
            range2.Cells.Font.Color = ColorTranslator.ToOle(Color.Black);

            //Зберігаємо документ
            excelApp.Application.ActiveWorkbook.SaveAs($"{fileName}.xlsx");
            //Закриваємо книгу
            workBook.Close(true);
            //Виходимо з додатку
            excelApp.Quit();
        }
    }
}
