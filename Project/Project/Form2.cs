﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string redName = textBox1.Text.ToString();
            string blueName = textBox2.Text.ToString();

            Form1 form1 = new Form1(redName,blueName);
            form1.ShowDialog();
        }
    }
}
