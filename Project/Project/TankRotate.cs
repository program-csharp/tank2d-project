﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    internal class TankRotate
    {
        static PictureBox PB;
        Image img;
        public TankRotate(PictureBox pB)
        {
            PB = pB;
            img = pB.Image;
        }
        public void imageAngle(byte lastKey,byte nowKey)
        {
            byte keyPressedLast = lastKey;
            byte keyPressedNow = nowKey;
            if (keyPressedNow == 1)
            {
                if(keyPressedLast == 2)
                {
                    img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                }
                else if(keyPressedLast == 3)
                {
                    img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                }
                else if (keyPressedLast == 4)
                {
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                }
            }
            else if (keyPressedNow == 2)
            {
                if (keyPressedLast == 1)
                {
                    img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                }
                else if (keyPressedLast == 3)
                {
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                }
                else if (keyPressedLast == 4)
                {
                    img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                }
            }
            else if (keyPressedNow == 3)
            {
                if (keyPressedLast == 1)
                {
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                }
                else if (keyPressedLast == 2)
                {
                    img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                }
                else if (keyPressedLast == 4)
                {
                    img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                }
            }
            else if (keyPressedNow == 4)
            {
                if (keyPressedLast == 1)
                {
                    img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                }
                else if (keyPressedLast == 2)
                {
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                }
                else if (keyPressedLast == 3)
                {
                    img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                }
            }
        }
    }
}
