﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Project
{
    public partial class Form1 : Form
    {
        //Змінні руху танків
        bool goLeft1, goRight1, goUp1, goDown1; //Змінні синього танку
        byte lastKey1 = 2; //Остання клавіша, яка була натиснута на клавіатурі, для синього танку
        byte nowKey1 = 0; //Клавіша, яка зараз натиснута на клавіатурі, для синього танку

        bool goLeft2, goRight2, goUp2, goDown2; //Змінні червоного танку
        byte lastKey2 = 1; //Остання клавіша, яка була натиснута на клавіатурі, для червоного танку
        byte nowKey2 = 0; //Клавіша, яка зараз натиснута на клавіатурі, для червоного танку

        int speed = 8; //Швидкість танків


        //Змінні руху патрона танків
        bool goBullet1Left, goBullet1Right, goBullet1Up, goBullet1Down = false; //Змінні патрона синього танку

        bool goBullet2Left, goBullet2Right, goBullet2Up, goBullet2Down = false; //Змінні патрона червоного танку

        int bulletSpeed = 25; //Швидкість патронів


        //Рахунок балів
        int blueCounter = 0; //Синього танку

        private void exportAsExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "xls files (.xlsx)|*.xlsx";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SaveToExcel.SaveDataToExcel(redName,blueName,redCounter,blueCounter,saveFileDialog1.FileName);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "txt files (.txt)|*.txt";
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter SW = new StreamWriter(saveFileDialog1.FileName);
                SW.WriteLine($"{redName} vs {blueName}: {redCounter}-{blueCounter}");
                SW.Close();
            }
        }

        int redCounter = 0; //Червоного танку

        //Імена гравців
        string blueName; //Синього танку
        string redName; //Червоного танку

        public Form1(string redName, string blueName)
        {
            this.blueName = blueName; //Записуємо ім'я гравця синього танку
            this.redName= redName; //Записуємо ім'я гравця червоного танку
            InitializeComponent();
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {   
            //Tanks
                //Blue tank
            picturePlayer2Tank.Left = 30; //По Ох
            picturePlayer2Tank.Top = 40; //По Оу
                //Red tank
            picturePlayer1Tank.Left = 940; //По Ох
            picturePlayer1Tank.Top = 600; //По Оу

            //Bullets 
                //Bullet of blue tank
            pictureBullet1.Left = -20; //По Ох
            pictureBullet1.Top = -20; //По Оу
                //Bullet2 of red tank
            pictureBullet2.Left = 2000; //По Ох
            pictureBullet2.Top = 1200; //По Оу

            //Включаємо таймер
            timerGame.Enabled = true;
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Обнуляємо рахунки для обидвох танків
            blueCounter = 0; //Синього
            redCounter = 0; //Червоного
            labelRed.Text = "Scores: " + redCounter.ToString();
            labelBlue.Text = "Scores: " + blueCounter.ToString();
            //Запускаємо код, який написаний у рестарті
            restartToolStripMenuItem_Click(sender, e);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pressKeyDown(object sender, KeyEventArgs e)
        {
            //Обертаємо танки
            TankRotate tankRotate1 = new TankRotate(picturePlayer1Tank); //Синій
            TankRotate tankRotate2 = new TankRotate(picturePlayer2Tank); //Червоний
            
            //Синій танк рухається
            if (e.KeyCode == Keys.Left)
            {
                goLeft1 = true;
                nowKey1 = 1;
                tankRotate1.imageAngle(lastKey1, nowKey1);
                lastKey1 = 1;
            }
            else if (e.KeyCode == Keys.Right)
            {
                goRight1 = true;
                nowKey1 = 2;
                tankRotate1.imageAngle(lastKey1, nowKey1);
                lastKey1 = 2;
            }
            else if (e.KeyCode == Keys.Up)
            {
                goUp1 = true;
                nowKey1 = 3;
                tankRotate1.imageAngle(lastKey1, nowKey1);
                lastKey1 = 3;
            }
            else if (e.KeyCode == Keys.Down)
            {
                goDown1 = true;
                nowKey1 = 4;
                tankRotate1.imageAngle(lastKey1, nowKey1);
                lastKey1 = 4;
            }
            else if (e.KeyCode == Keys.Enter)
            {
                if (nowKey1 == 1)
                {
                    pictureBullet1.Left = picturePlayer1Tank.Left-(2+pictureBullet1.Width);
                    pictureBullet1.Top = picturePlayer1Tank.Top + picturePlayer1Tank.Height / 2 - pictureBullet1.Height/2;
                    goBullet1Left = true;
                }
                else if (nowKey1 == 2)
                {
                    pictureBullet1.Left = picturePlayer1Tank.Left + picturePlayer1Tank.Width+2;
                    pictureBullet1.Top = picturePlayer1Tank.Top + picturePlayer1Tank.Height / 2 - pictureBullet1.Height / 2;
                    goBullet1Right = true;
                }
                else if (nowKey1 == 3)
                {
                    pictureBullet1.Left = picturePlayer1Tank.Left + picturePlayer1Tank.Width / 2 - pictureBullet1.Width / 2;
                    pictureBullet1.Top = picturePlayer1Tank.Top- (2 + pictureBullet1.Height);
                    goBullet1Up = true;
                }
                else if (nowKey1 == 4)
                {
                    pictureBullet1.Left = picturePlayer1Tank.Left + picturePlayer1Tank.Width / 2 - pictureBullet1.Width / 2;
                    pictureBullet1.Top = picturePlayer1Tank.Top + picturePlayer1Tank.Height + 2;
                    goBullet1Down = true;
                }
            }

            //Червоний танк рухається
            if (e.KeyCode == Keys.A)
            {
                goLeft2 = true;
                nowKey2 = 1;
                tankRotate2.imageAngle(lastKey2, nowKey2);
                lastKey2 = 1;
            }
            else if (e.KeyCode == Keys.D)
            {
                goRight2 = true;
                nowKey2 = 2;
                tankRotate2.imageAngle(lastKey2, nowKey2);
                lastKey2 = 2;
            }
            else if (e.KeyCode == Keys.W)
            {
                goUp2 = true;
                nowKey2 = 3;
                tankRotate2.imageAngle(lastKey2, nowKey2);
                lastKey2 = 3;
            }
            else if (e.KeyCode == Keys.S)
            {
                goDown2 = true;
                nowKey2 = 4;
                tankRotate2.imageAngle(lastKey2, nowKey2);
                lastKey2 = 4;
            }
            else if (e.KeyCode == Keys.Space)
            {
                if (nowKey2 == 1)
                {
                    pictureBullet2.Left = picturePlayer2Tank.Left-(2 + pictureBullet2.Width);
                    pictureBullet2.Top = picturePlayer2Tank.Top + picturePlayer2Tank.Height / 2 - pictureBullet2.Height / 2;
                    goBullet2Left = true;
                }
                else if (nowKey2 == 2)
                {
                    pictureBullet2.Left = picturePlayer2Tank.Left + picturePlayer2Tank.Width+2;
                    pictureBullet2.Top = picturePlayer2Tank.Top + picturePlayer2Tank.Height / 2 - pictureBullet2.Height / 2;
                    goBullet2Right = true;
                }
                else if (nowKey2 == 3)
                {
                    pictureBullet2.Left = picturePlayer2Tank.Left + picturePlayer2Tank.Width / 2 - pictureBullet2.Width/2;
                    pictureBullet2.Top = picturePlayer2Tank.Top - (2 + pictureBullet2.Height);
                    goBullet2Up = true;
                }
                else if (nowKey2 == 4)
                {
                    pictureBullet2.Left = picturePlayer2Tank.Left + picturePlayer2Tank.Width / 2 - pictureBullet2.Width / 2;
                    pictureBullet2.Top = picturePlayer2Tank.Top + picturePlayer2Tank.Height+2 ;
                    goBullet2Down = true;
                }
            }
        }

        private void pressKeyUp(object sender, KeyEventArgs e)
        {
            //Синій танк припиняє рух
            if (e.KeyCode == Keys.Left)
            {
                goLeft1 = false;
            }
            else if (e.KeyCode == Keys.Right)
            {
                goRight1 = false;
            }
            else if (e.KeyCode == Keys.Up)
            {
                goUp1 = false;
            }
            else if (e.KeyCode == Keys.Down)
            {
                goDown1 = false;
            }

            //Червоний танк припиняє рух 
            if (e.KeyCode == Keys.A)
            {
                goLeft2 = false;
            }
            else if (e.KeyCode == Keys.D)
            {
                goRight2 = false;
            }
            else if (e.KeyCode == Keys.W)
            {
                goUp2 = false;
            }
            else if (e.KeyCode == Keys.S)
            {
                goDown2 = false;
            }
        }
        private void timerGame_Tick(object sender, EventArgs e)
        {
            //Players walk
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && ((string)x.Tag == "block" || (string)x.Tag == "bullet"||(string)x.Tag == "Tank"))
                {
                    if ((string)x.Tag != "Tank")
                    {
                        if (picturePlayer1Tank.Bounds.IntersectsWith(x.Bounds))
                        {
                            goLeft1 = false;
                            goRight1 = false;
                            goUp1 = false;
                            goDown1 = false;
                            timerGame.Enabled = false;
                            MessageBox.Show(blueName.ToString()+", your tank was broken ");
                            redCounter++;
                            labelRed.Text = "Scores: " + redCounter.ToString();
                        }
                        if (picturePlayer2Tank.Bounds.IntersectsWith(x.Bounds))
                        {
                            goLeft2 = false;
                            goRight2 = false;
                            goUp2 = false;
                            goDown2 = false;
                            timerGame.Enabled = false;
                            MessageBox.Show(redName.ToString()+", your tank was broken ");
                            blueCounter++;
                            labelBlue.Text = "Scores: " + blueCounter.ToString();
                        }
                    
                    }
                    if ((string)x.Tag != "bullet")
                    {
                        if (pictureBullet1.Bounds.IntersectsWith(x.Bounds))
                        {
                            goBullet1Left = false;
                            goBullet1Right = false;
                            goBullet1Up = false;
                            goBullet1Down = false;
                            pictureBullet1.Left = -10;
                            pictureBullet1.Top = -10;
                        }
                        if (pictureBullet2.Bounds.IntersectsWith(x.Bounds))
                        {
                            goBullet2Left = false;
                            goBullet2Right = false;
                            goBullet2Up = false;
                            goBullet2Down = false;
                            pictureBullet2.Left = 2000;
                            pictureBullet2.Top = 1200;
                        }
                    }
                }
            }

            //Tanks walk
                //Tank1
            TankWalk.tankWalk(goLeft1, goRight1, goUp1, goDown1, picturePlayer1Tank, speed, ClientSize.Width, ClientSize.Height);
                //Tank2
            TankWalk.tankWalk(goLeft2, goRight2, goUp2, goDown2, picturePlayer2Tank, speed, ClientSize.Width, ClientSize.Height);

            //Bullets fly
                //Bullet1
            TankWalk.bulletWalk(goBullet1Left, goBullet1Right, goBullet1Up, goBullet1Down, pictureBullet1, bulletSpeed, ClientSize.Width, ClientSize.Height);
                //Bullet2
            TankWalk.bulletWalk(goBullet2Left, goBullet2Right, goBullet2Up, goBullet2Down, pictureBullet2, bulletSpeed, ClientSize.Width, ClientSize.Height);
        }
    }
}
